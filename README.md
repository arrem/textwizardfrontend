# TextWizard Backend

This project contains the frontend for the TextWizard application, written in Angular 15.0.3 and TypeScript 4.8.4, as well as the Angular Material 15.0.2 library for the UI components.

This file contains some guidelines for how to best read the project.

## Application Overview

The screenshot below provides an overview of the application in its current state.
![](preview.png)

The supported features and design goals of the application are as follows:
* Ability to enter a text and analyze it with the press of a button.
* Ability to switch between counting consonants or vowels.
* Ability to choose between an online operation mode (via the backend) or local processing, as well as prompting the user to switch to offline processing when the server cannot be reached.
* Display of results in a sortable tabular format, and an option to copy them to clipboard.
* Full view of the usage history, as well as an option to see past results.
* Support for mobile layouts.
* Support for screen reader usage (tested with TalkBack on Android 12).

## The Analysis Service

The services that this application uses can be found in the `src/app/services` directory. The main service used to analyze text is the `text-analysis.service.ts` class.

This service in turn calls either the online analysis service that can be found in `online-text-analysis.service.ts` or the offline (local) analysis service, implemented in `offline-text-analysis.service.ts` (and appropriately tested in `offline-text-analysis-service.spec.ts`).

## The Components
The application is organized into multiple components, which can all be found under the `src/app/components` directory. We provide a brief overview of each component's purpose and responsibility here, though each class is documented with a JDoc that goes into more detail.

| Component path                 | Description                                                                                         |
|--------------------------------|-----------------------------------------------------------------------------------------------------|
| `analysis-form`                | The main form that lets the user enter their text, choose options and run the analysis.             |
| `analysis-results`             | The card that displays the results of the analysis in tabular form and offers options to copy them. |
| `history-view`                 | The sidebar for displaying the history of user actions.                                             |
| `history-view-item`            | Helper component for displaying individual cards within the history.                                |
| `copy-snackbar`                | A snackbar that notifies the user that their result has been copied.                                |
| `switch-operation-mode-dialog` | A dialog that prompts the user to switch to offline mode when the server returns an error.          |
