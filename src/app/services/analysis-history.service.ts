import { Injectable } from '@angular/core';
import { AnalysisResult } from '../model/AnalysisResult';
import { Subject } from 'rxjs';

/**
 * The `AnalysisHistoryService` provides functionality for keeping track of past analysis results, as well as getting
 * paginated views of these results.
 *
 * New items can be pushed to the service using the {@link pushItem} function, which will also fire the
 * {@link newItemPushed} event to notify any listeners that there has been an update in the analysis history.
 * Pagination is controlled via the {@link updatePageIndex} method.
 */
@Injectable({
    providedIn: 'root'
})
export class AnalysisHistoryService {
    private history: AnalysisResult[] = [];

    public newItemPushed = new Subject<AnalysisResult[]>();
    public itemViewRequested = new Subject<AnalysisResult>();

    private _pageSize = 4
    private _pageIndex = 0

    /**
     * Gets the total length of all items in this object. This value is unaffected by paging.
     */
    public get totalLength(): number {
        return this.history.length;
    }

    /**
     * Gets the page size for this object.
     */
    public get pageSize(): number {
        return this._pageSize;
    }

    /**
     * Gets the currently selected (0-based) page index for this object.
     */
    public get pageIndex(): number {
        return this._pageIndex;
    }

    /**
     * Computes the starting index of the currently selected page in the {@link history} array, that can be used
     * to slice the array and generate a page.
     *
     * @see toIndex
     */
    private get fromIndex(): number {
        return this._pageIndex * this._pageSize
    }

    /**
     * Computes the ending index of the currently selected page in the {@link history} array, that can be used
     * to slice the array and generate a page.
     *
     * @see fromIndex
     */
    private get toIndex(): number {
        return this.fromIndex + this._pageSize
    }

    /**
     * Pushes the given {@link AnalysisResult} to the end of this service's history record, and emits a
     * {@link newItemPushed} event with the currently displayed page contents to notify listeners of the change.
     */
    pushItem(result: AnalysisResult): void {
        this.history.push(result);

        this.newItemPushed.next(this.history.slice(this.fromIndex, this.toIndex));
    }

    /**
     * Emits an {@link itemViewRequested} event to notify listeners that there has been a request to view the
     * given {@link AnalysisResult}. In practice, this event is fired by the history view when an item has been
     * clicked on, and it is subscribed to by the form, which updates its contents accordingly.
     */
    requestItemView(item: AnalysisResult): void {
        this.itemViewRequested.next(item);
    }

    /**
     * Sets the current page index of the service to the given `pageIndex`, and returns the currently displayed page.
     *
     * @param pageIndex The new page index to display. This value must not be less than zero or exceed the maximum
     *                  number of pages that the service can display based on its total length and page size.
     *
     * @returns An array of {@link AnalysisResult} objects representing the history page that is currently active.
     *
     * @throws If the given `pageIndex` is out of bounds.
     */
    updatePageIndex(pageIndex: number): AnalysisResult[] {
        if (pageIndex < 0 && pageIndex > Math.ceil(this.history.length / this.pageSize)) {
            throw `Illegal argument to updatePageIndex, page ${pageIndex} is out of range.`;
        }

        this._pageIndex = pageIndex;

        return this.history.slice(this.fromIndex, this.toIndex);
    }
}
