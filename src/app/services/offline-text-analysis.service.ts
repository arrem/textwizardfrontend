import { ITextAnalysisService } from '../model/ITextAnalysisService';
import { Observable, of } from 'rxjs';
import { AnalysisCount } from '../model/AnalysisCount';

/**
 * The `OfflineTextAnalysisService` is an implementation of the {@link ITextAnalysisService} interface, which runs its
 * analysis offline, that is, in the browser using TypeScript, and therefore it does not require an active internet
 * connection to perform the analysis.
 *
 * Note that this class is only a helper class and is not directly injectable, and should not be used in user code.
 *
 * @see TextAnalysisService
 */
export class OfflineTextAnalysisService implements ITextAnalysisService {
    private vowels = new Set('AEIOU');

    analyzeText(text: string, mode: 'Consonants' | 'Vowels'): Observable<AnalysisCount[]> {
        const map = new Map();

        for (const char of text) {
            const upper = char.toLocaleUpperCase('en-US');

            // Even with toLocaleUpperCase, JS will convert ß to SS, so we reject everything that is too long, or out
            // of the ASCII range upfront.
            if (upper.length > 1 || upper < 'A' || upper > 'Z') {
                continue;
            }

            // A character is added to our map if either:
            //   1. It is a vowel, and we are interested in vowels.
            //   2. It is a consonant, and we are interested in consonants.
            const countsAsVowel = mode === 'Vowels' && this.vowels.has(upper);
            const countsAsConsonant = mode === 'Consonants' && !this.vowels.has(upper);

            if (countsAsVowel || countsAsConsonant) {
                const existing = map.get(upper) ?? 0;
                map.set(upper, existing + 1);
            }
        }

        const result: AnalysisCount[] = [];

        map.forEach((value, key) => {
            result.push({ key: key, value: value });
        });

        return of(result);
    }
}
