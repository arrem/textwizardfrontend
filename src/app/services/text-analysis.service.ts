import { Injectable } from '@angular/core';
import { OnlineTextAnalysisService } from './online-text-analysis.service';
import { OfflineTextAnalysisService } from './offline-text-analysis.service';
import { Observable } from 'rxjs';
import { OperationMode } from '../model/OperationMode';
import { ITextAnalysisService } from '../model/ITextAnalysisService';
import { AnalysisCount } from '../model/AnalysisCount';

/**
 * The `TextAnalysisService` provides functionality to analyze a given text and count the consonants or the vowels
 * within it. This class ties together two different implementations, namely an online service which does the
 * processing on a server, and an offline service which processes the text locally in the browser.
 *
 * @see analyzeText
 */
@Injectable()
export class TextAnalysisService {

    constructor(
        private onlineService: OnlineTextAnalysisService,
        private offlineService: OfflineTextAnalysisService
    ) {
    }

    /**
     * Computes the counts of characters of a certain type in a piece of text.
     *
     * @param text A string containing the text that is to be analyzed.
     * @param mode A string indicating whether the text's consonants or its vowels should be counted.
     * @param operationMode An enum indicating whether the analysis should run offline (locally) or online (remotely).
     * @returns An {@link Observable} of an array of {@link AnalysisCount} objects, containing the counts for each
     *          character that matches the selected `mode` within the provided `text`.
     */
    analyzeText(text: string, mode: 'Consonants' | 'Vowels', operationMode: OperationMode): Observable<AnalysisCount[]> {
        return this.getService(operationMode).analyzeText(text, mode);
    }

    private getService(operationMode: OperationMode): ITextAnalysisService {
        if (operationMode == OperationMode.Online) {
            return this.onlineService;
        }

        return this.offlineService;
    }
}
