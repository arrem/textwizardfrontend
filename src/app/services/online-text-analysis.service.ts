import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { ITextAnalysisService } from '../model/ITextAnalysisService';
import { environment } from '../../environment/environment';
import { AnalysisCount } from '../model/AnalysisCount';

/**
 * The `OnlineTextAnalysisService` is an implementation of the {@link ITextAnalysisService} interface, which runs its
 * analysis on a backend server. In particular, this class will look for the {@link environment.apiURL} property to
 * decide which backend to contact, and will return the result produced by this backend.
 *
 * Note that this class is only a helper class and is not directly injectable, and should not be used in user code.
 *
 * @see TextAnalysisService
 */
export class OnlineTextAnalysisService implements ITextAnalysisService {
    constructor(
        private httpClient: HttpClient
    ) {
    }

    analyzeText(text: string, mode: 'Consonants' | 'Vowels'): Observable<AnalysisCount[]> {
        return this.httpClient
            .post(`${environment.apiURL}/api/analyze`, { 'text': text, 'mode': mode })
            .pipe(map(e => e as AnalysisCount[]));
    }
}
