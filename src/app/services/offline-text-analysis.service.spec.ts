import { TestBed, waitForAsync } from '@angular/core/testing';
import { OfflineTextAnalysisService } from './offline-text-analysis.service';

describe('OfflineTextAnalysisService', () => {
    let service: OfflineTextAnalysisService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = new OfflineTextAnalysisService();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should produce an empty map when asked for consonants in an empty text', waitForAsync(() => {
        service.analyzeText('', "Consonants")
            .subscribe(result => expect(result.length).toBe(0));
    }));

    it('should produce an empty map when asked for vowels in an empty text', waitForAsync(() => {
        service.analyzeText('', "Vowels")
            .subscribe(result => expect(result.length).toBe(0));
    }));

    it('should produce an empty map when asked for consonants in non-ascii text', waitForAsync(() => {
        const text = ' \r\n\t  \uD83D\uDC6E\u200D\u2640\uFE0F öüäß 1234567890';
        service.analyzeText(text, "Consonants")
            .subscribe(result => {
                expect(result.length).toBe(0);
            });
    }));

    it('should produce an empty map when asked for vowels in non-ascii text', waitForAsync(() => {
        const text = ' \r\n\t  \uD83D\uDC6E\u200D\u2640\uFE0F öüäß 1234567890';
        service.analyzeText(text, "Vowels")
            .subscribe(result => expect(result.length).toBe(0));
    }));

    it('should correctly handle text with only vowels', waitForAsync(() => {
        const text = 'aeiouAEIOUaoOoAe';
        service.analyzeText(text, "Vowels")
            .subscribe(result => {
                expect(result.length).toBe(5);
                expect(result.find(e => e.key == 'A')?.value).toBe(4);
                expect(result.find(e => e.key == 'E')?.value).toBe(3);
                expect(result.find(e => e.key == 'I')?.value).toBe(2);
                expect(result.find(e => e.key == 'O')?.value).toBe(5);
                expect(result.find(e => e.key == 'U')?.value).toBe(2);
            });
    }));

    it('should correctly handle text with only consonants', waitForAsync(() => {
        const baseConsonants = 'bcdfghjklmnpqrstvwxyz';
        const text = baseConsonants + baseConsonants.toUpperCase();

        service.analyzeText(text, "Consonants")
            .subscribe(result => {
                expect(result.length).toBe(21);

                for (const consonant of baseConsonants) {
                    const upper = consonant.toUpperCase();
                    expect(result.find(e => e.key == upper)?.value).toBe(2);
                }
            });
    }));

    it('should correctly count vowels in a mixed string', waitForAsync(() => {
        const text = 'The quick brown fox jumps over the lazy dog.';

        service.analyzeText(text, "Vowels")
            .subscribe(result => {
                expect(result.length).toBe(5);
                expect(result.find(e => e.key == 'A')?.value).toBe(1);
                expect(result.find(e => e.key == 'E')?.value).toBe(3);
                expect(result.find(e => e.key == 'I')?.value).toBe(1);
                expect(result.find(e => e.key == 'O')?.value).toBe(4);
                expect(result.find(e => e.key == 'U')?.value).toBe(2);
            });
    }));

    it('should correctly determine the number of consonants in a mixed string', waitForAsync(() => {
        const text = 'The quick brown fox jumps over the lazy dog.';

        service.analyzeText(text, "Consonants")
            .subscribe(result => {
                expect(result.length).toBe(21);
            });
    }));
});
