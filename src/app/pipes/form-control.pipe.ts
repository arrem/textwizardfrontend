import { Pipe, PipeTransform } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';

/**
 * The `FormControlPipe` casts an {@link AbstractControl} into an appropriately typed {@link FormControl}, which
 * can then be tied to form controls in component templates.
 */
@Pipe({
  name: 'formControl'
})
export class FormControlPipe implements PipeTransform {
  transform(value: AbstractControl): FormControl<typeof value['value']> {
    return value as FormControl<typeof value['value']>;
  }

}
