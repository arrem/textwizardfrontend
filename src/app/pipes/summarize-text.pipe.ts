import { Pipe, PipeTransform } from '@angular/core';

/**
 * The `SummarizeTextPipe` is used to shorten a text. If the text is less than 48 characters long, it is returned
 * unchanged. Otherwise, the first 48 characters of the text are returned, followed by an ellipsis character.
 */
@Pipe({
    name: 'summarizeText'
})
export class SummarizeTextPipe implements PipeTransform {

    private maxSize = 48

    transform(value: string): string {
        if (value.length < this.maxSize) {
            return value;
        }

        return value.slice(0, this.maxSize) + "…";
    }
}
