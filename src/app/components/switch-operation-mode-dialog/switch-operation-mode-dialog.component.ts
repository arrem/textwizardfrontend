import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

/**
 * The `SwitchOperationModeDialogComponent` is responsible for displaying a dialog that prompts the user to switch to
 * offline mode when the requests on the server encounter an error.
 */
@Component({
  selector: 'app-switch-operation-mode-dialog',
  templateUrl: './switch-operation-mode-dialog.component.html',
  styleUrls: ['./switch-operation-mode-dialog.component.css']
})
export class SwitchOperationModeDialogComponent {
    constructor(
        private dialogRef: MatDialogRef<SwitchOperationModeDialogComponent>
    ) {
    }

    closeDialog(shouldSwitch: boolean): void {
        this.dialogRef.close(shouldSwitch);
    }
}
