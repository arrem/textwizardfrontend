import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchOperationModeDialogComponent } from './switch-operation-mode-dialog.component';
import { AppModule } from '../../app.module';

describe('SwitchOperationModeDialogComponent', () => {
    let component: SwitchOperationModeDialogComponent;
    let fixture: ComponentFixture<SwitchOperationModeDialogComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [AppModule],
            declarations: [SwitchOperationModeDialogComponent]
        })
            .compileComponents();

        fixture = TestBed.createComponent(SwitchOperationModeDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
