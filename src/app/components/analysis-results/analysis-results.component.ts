import { Component, Input, OnInit } from '@angular/core';
import { AnalysisCount } from '../../model/AnalysisCount';
import { Sort } from '@angular/material/sort';
import { AnalysisResult } from '../../model/AnalysisResult';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CopySnackbarComponent } from '../copy-snackbar/copy-snackbar.component';
import { A, E } from '@angular/cdk/keycodes';

/**
 * The `AnalysisResultsComponent` is responsible for rendering the results of a text analysis. In particular, it
 * displays a card that contains a sortable table with the individual letters and their occurrences, assuming that
 * there were some in the text. If no occurrences of the requested letter category were found, the component will
 * display a warning message instead of rendering a table.
 *
 * The component's contents are set using the {@link result} input property.
 */
@Component({
    selector: 'app-analysis-results',
    templateUrl: './analysis-results.component.html',
    styleUrls: ['./analysis-results.component.css']
})
export class AnalysisResultsComponent implements OnInit {
    private mostRecentSort: Sort | null = null;
    private _result: AnalysisResult | null = null;

    /**
     * Sets the {@link AnalysisResult} that this component should render. Setting this value will preserve the sorting
     * method that had been used before, if any.
     */
    @Input()
    set result(value: AnalysisResult | null) {
        this._result = value;
        this.onSortChange(null);
    }

    get result(): AnalysisResult | null {
        return this._result;
    }

    sortedResults: AnalysisCount[] = [];

    constructor(
        private snackBar: MatSnackBar
    ) {
    }

    ngOnInit(): void {
        this.sortedResults = this.result?.response ?? [];
    }


    /**
     * Resorts the data. The value can either be a new sort, when the function is being invoked from the table's sort
     * event, or it is null when we manually trigger sorting because the data has been updated.
     *
     * In case `null` is provided, the method will attempt to preserve the most recent sorting strategy if one is
     * available.
     */
    onSortChange(sort: Sort | null) {
        const actualSort = sort ?? this.mostRecentSort;

        // If there is no sort information, our only option is to fall back to not sorting at all and
        // just copy over the array as it is.
        if (actualSort == null) {
            this.sortedResults = this.result?.response.slice() ?? [];
            return;
        }

        if (sort !== null) {
            this.mostRecentSort = sort;
        }

        const data = this.result?.response.slice();

        if (data === undefined) {
            return;
        }

        data.sort((first, second) => {
            const ascending = actualSort.direction === 'asc';

            switch (actualSort.active) {
                case 'letter':
                    return this.compare(first.key, second.key, ascending);
                case 'occurrences':
                    return this.compare(first.value, second.value, ascending);
                default:
                    return 0;
            }
        });

        this.sortedResults = data;
    }

    private compare<T>(first: T, second: T, ascending: boolean): number {
        if (first === second) {
            return 0;
        }

        const sortDirectionMultiplier = ascending ? 1 : -1;
        const order = (first < second) ? -1 : 1;

        return order * sortDirectionMultiplier;
    }

    copyContents(mode: 'csv' | 'json'): void {
        let data;
        let message = '';

        if (mode == 'json') {
            data = JSON.stringify(this.sortedResults);
            message = 'Your results were copied in JSON format!'
        } else {
            data = this.sortedResults.map(element => `${element.key},${element.value}`).join('\n');
            message = 'Your results were copied in CSV format!'
        }


        navigator.clipboard
            .writeText(data)
            .then(() => {
                this.snackBar.openFromComponent(CopySnackbarComponent, { duration: 1500, data: message });
            });
    }
}
