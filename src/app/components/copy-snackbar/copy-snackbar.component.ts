import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';

/**
 * The `CopySnackbarComponent` provides a snackbar that the result display uses to confirm that the copy action has
 * succeeded. The text to display can be passed via the data parameter when the snackbar is created.
 */
@Component({
    selector: 'app-copy-snackbar',
    templateUrl: './copy-snackbar.component.html',
    styleUrls: ['./copy-snackbar.component.css']
})
export class CopySnackbarComponent {
    constructor(
        @Inject(MAT_SNACK_BAR_DATA) public content: string,
        public snackBarRef: MatSnackBarRef<CopySnackbarComponent>,
    ) {
    }
}
