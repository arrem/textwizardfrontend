import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CopySnackbarComponent } from './copy-snackbar.component';

describe('CopySnackbarComponent', () => {
    let component: CopySnackbarComponent;
    let fixture: ComponentFixture<CopySnackbarComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [CopySnackbarComponent]
        })
            .compileComponents();

        fixture = TestBed.createComponent(CopySnackbarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
