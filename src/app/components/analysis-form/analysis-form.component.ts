import { Component, EventEmitter, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AnalysisFormData } from '../../model/AnalysisFormData';
import { TextAnalysisService } from '../../services/text-analysis.service';
import { OperationMode } from '../../model/OperationMode';
import { AnalysisResult } from '../../model/AnalysisResult';
import { AnalysisHistoryService } from '../../services/analysis-history.service';

/**
 * The `AnalysisFormComponent` is responsible for rendering the form that lets the user input text and run analysis on
 * it. The component provides a text box for text entry, controls to choose which type of characters to count, and
 * controls to choose whether the analysis is run online or offline.
 *
 * The component interacts with its parent component via the two events it emits.
 *
 * @see analysisComplete
 * @see analysisFailed
 */
@Component({
    selector: 'app-analysis-form',
    templateUrl: './analysis-form.component.html',
    styleUrls: ['./analysis-form.component.css']
})
export class AnalysisFormComponent {

    formGroup: FormGroup;
    text: AbstractControl;
    mode: AbstractControl;
    operationMode: AbstractControl;

    /**
     * This event is fired when the user submits a text for analysis, and the analysis succeeds and produces a
     * results. The produced result is an array of {@link AnalysisCount} objects.
     */
    @Output() analysisComplete: EventEmitter<AnalysisResult> = new EventEmitter<AnalysisResult>();
    /**
     * This event is fired when the user submits a text for analysis, but the analysis throws an error instead of
     * completing successfully. The produced result is an error object containing a description of the error, which
     * can be used to handle it appropriately.
     */
    @Output() analysisFailed: EventEmitter<any> = new EventEmitter<any>();

    constructor(
        builder: FormBuilder,
        private textAnalysisService: TextAnalysisService,
        private analysisHistoryService: AnalysisHistoryService,
    ) {
        this.formGroup = builder.group({
            'text': ['', Validators.required],
            'mode': ['Vowels', Validators.required],
            'operationMode': ['online', Validators.required],
        });

        this.text = this.formGroup.controls['text'];
        this.mode = this.formGroup.controls['mode'];
        this.operationMode = this.formGroup.controls['operationMode'];

        this.analysisHistoryService.itemViewRequested.subscribe((item) => {
            this.analysisComplete.emit(item);
        });
    }

    onFormSubmit(data: AnalysisFormData): void {
        if (!this.formGroup.valid) {
            return;
        }

        const mode = data.operationMode === 'online' ? OperationMode.Online : OperationMode.Offline;

        this.textAnalysisService.analyzeText(data.text, data.mode, mode)
            .subscribe({
                next: result => {
                    const actual = Array.isArray(result) ? result : [];
                    const item: AnalysisResult = { request: data, response: actual };

                    this.analysisHistoryService.pushItem(item);
                    this.analysisComplete.emit(item);
                },
                error: error => this.analysisFailed.emit(error),
            });
    }

    /**
     * Switches on the offline operation mode for this form and resubmits it with the currently present data.
     */
    switchToOfflineMode(): void {
        this.operationMode.setValue('offline');
        this.onFormSubmit(this.formGroup.value);
    }

    /**
     * Updates the data displayed by this form to the provided {@link AnalysisFormData}. By design, this method will
     * not override the operation mode, regardless of what it is set to in the `data` argument.
     *
     * @param data The new data for the form to use.
     */
    updateData(data: AnalysisFormData): void {
        this.text.setValue(data.text);
        this.mode.setValue(data.mode);
    }
}
