import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryViewItemComponent } from './history-view-item.component';

describe('HistoryViewItemComponent', () => {
  let component: HistoryViewItemComponent;
  let fixture: ComponentFixture<HistoryViewItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryViewItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistoryViewItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
