import { Component, Input } from '@angular/core';
import { AnalysisResult } from '../../model/AnalysisResult';

/**
 * The `HistoryViewItemComponent` is responsible for rendering the individual history items of a
 * {@link HistoryViewComponent}.
 *
 * It renders a card containing the (potentially shortened) request text, and the analysis mode.
 */
@Component({
  selector: 'app-history-view-item',
  templateUrl: './history-view-item.component.html',
  styleUrls: ['./history-view-item.component.css']
})
export class HistoryViewItemComponent {
    @Input() item?: AnalysisResult
}
