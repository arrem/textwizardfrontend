import { Component, OnInit } from '@angular/core';
import { AnalysisHistoryService } from '../../services/analysis-history.service';
import { AnalysisResult } from '../../model/AnalysisResult';
import { PageEvent } from '@angular/material/paginator';

/**
 * The `HistoryViewComponent` is responsible for displaying the result history part of the application, including the
 * header, the list of items and a paginator. Individual history items can be clicked on to bring up their results.
 *
 * The component makes use of the {@link AnalysisHistoryService} to keep track of its items and paginate them.
 */
@Component({
    selector: 'app-history-view',
    templateUrl: './history-view.component.html',
    styleUrls: ['./history-view.component.css']
})
export class HistoryViewComponent implements OnInit {

    history: AnalysisResult[] = []

    constructor(
        public analysisHistoryService: AnalysisHistoryService
    ) {
    }

    ngOnInit(): void {
        // The newItemPushed event is fired by the analysis form when it successfully evaluates an item, so we update
        // our contents to the new state.
        this.analysisHistoryService.newItemPushed.subscribe((newHistory: AnalysisResult[]) => {
            this.history = newHistory;
        });
    }

    /**
     * Requests that the given {@link AnalysisResult} is displayed on the screen. This will show the output of the
     * result, and its original text.
     */
    loadItem(item: AnalysisResult): void {
        this.analysisHistoryService.requestItemView(item);
    }

    onPageRequest(event: PageEvent) {
        this.history = this.analysisHistoryService.updatePageIndex(event.pageIndex);
    }
}
