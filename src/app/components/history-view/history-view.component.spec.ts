import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryViewComponent } from './history-view.component';
import { AppModule } from '../../app.module';

describe('HistoryViewComponent', () => {
    let component: HistoryViewComponent;
    let fixture: ComponentFixture<HistoryViewComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [AppModule],
            declarations: [HistoryViewComponent]
        })
            .compileComponents();

        fixture = TestBed.createComponent(HistoryViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
