import { Component, ViewChild } from '@angular/core';
import { AnalysisResult } from './model/AnalysisResult';
import { MatDialog } from '@angular/material/dialog';
import {
    SwitchOperationModeDialogComponent
} from './components/switch-operation-mode-dialog/switch-operation-mode-dialog.component';
import { AnalysisFormComponent } from './components/analysis-form/analysis-form.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    result: AnalysisResult | null = null;

    @ViewChild(AnalysisFormComponent) form?: AnalysisFormComponent;

    constructor(
        private dialog: MatDialog,
    ) {
    }


    onAnalysisComplete(result: AnalysisResult): void {
        this.result = result;
        this.form?.updateData(result.request);
    }

    onAnalysisFailed(): void {
        this.dialog.open(SwitchOperationModeDialogComponent)
            .afterClosed()
            .subscribe(shouldSwitch => {
            // Value is undefined when the dialog is closed without pressing a button, so we default to false there.
            shouldSwitch = shouldSwitch || false;

            if (shouldSwitch) {
                this.form?.switchToOfflineMode();
            }
        });
    }
}
