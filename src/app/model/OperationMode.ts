/**
 * The `OperationMode` enum captures the two different operation modes that the application supports.
 *
 * @see Online
 * @see Offline
 */
export enum OperationMode {
    /**
     * Online operation mode, indicating that the analysis should be done on a remote server.
     */
    Online,
    /**
     * Offline operation mode, indicating that the analysis should be done in the browser.
     */
    Offline,
}
