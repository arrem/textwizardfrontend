import { AnalysisFormData } from './AnalysisFormData';
import { AnalysisCount } from './AnalysisCount';

/**
 * The `AnalysisResult` interface represents the structure of an object used to store the result of a text analysis
 * request. It stores both the {@link AnalysisCount} array returned by the server, as well as the information about
 * the request stored in {@link AnalysisFormData}.
 *
 * @see request
 * @see response
 */
export interface AnalysisResult {
    /**
     * The analysis request as sent by the client, containing all the relevant form data.
     */
    request: AnalysisFormData,
    /**
     * The array of counts for each of the requested characters, as returned by the server.
     */
    response: AnalysisCount[],
}
