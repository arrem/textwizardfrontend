import { Observable } from 'rxjs';
import { AnalysisCount } from './AnalysisCount';

/**
 * The `ITextAnalysisService` describes the common method for analyzing text that is implemented by its two descendants.
 *
 * @see analyzeText
 * @see OfflineTextAnalysisService
 * @see OnlineTextAnalysisService
 */
export interface ITextAnalysisService {
    /**
     * Computes the counts of characters of a certain type in a piece of text.
     *
     * @param text A string containing the text that is to be analyzed.
     * @param mode A string indicating whether the text's consonants or its vowels should be counted.
     * @returns An {@link Observable} of an array of {@link AnalysisCount} objects, containing the counts for each
     *          character that matches the selected `mode` within the provided `text`.
     */
    analyzeText(text: string, mode: "Consonants" | "Vowels"): Observable<AnalysisCount[]>
}
