/**
 * The `AnalysisFormData` interface captures the fields returned by the text analysis form of the
 * {@link AnalysisFormComponent}.
 *
 * @see text
 * @see mode
 * @see operationMode
 */
export interface AnalysisFormData {

    /**
     * The text that the user submitted for evaluation.
     */
    text: string,

    /**
     * The analysis mode that the user has selected. This value is represented as a string for convenience, as that
     * is the form expected by the online analysis backend.
     */
    mode: 'Vowels' | 'Consonants',

    /**
     * The operation mode that the user hs selected, which controls whether the analysis is done on the backend
     * server or locally in the browser.
     */
    operationMode: 'online' | 'offline'
}
