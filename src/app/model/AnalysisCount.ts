/**
 * The `AnalysisCount` interface represents the structure of a result object returned by the text analysis backend.
 * This object is composed of two properties, one representing a character, and the other representing its number of
 * occurrences within the analyzed text.
 *
 * @see key
 * @see value
 */
export interface AnalysisCount {
    /**
     * A key containing the character whose count is being represented by this object.
     */
    key: string,
    /**
     * The number of occurrences of the given character within the analyzed text.
     */
    value: number
}
