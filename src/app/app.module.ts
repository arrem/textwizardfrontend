import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AnalysisFormComponent } from './components/analysis-form/analysis-form.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextAnalysisService } from './services/text-analysis.service';
import { OnlineTextAnalysisService } from './services/online-text-analysis.service';
import { OfflineTextAnalysisService } from './services/offline-text-analysis.service';
import { AnalysisResultsComponent } from './components/analysis-results/analysis-results.component';
import { FormControlPipe } from './pipes/form-control.pipe';
import { MatSortModule } from '@angular/material/sort';
import { SwitchOperationModeDialogComponent } from './components/switch-operation-mode-dialog/switch-operation-mode-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { CopySnackbarComponent } from './components/copy-snackbar/copy-snackbar.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HistoryViewComponent } from './components/history-view/history-view.component';
import { HistoryViewItemComponent } from './components/history-view-item/history-view-item.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { SummarizeTextPipe } from './pipes/summarize-text.pipe';


@NgModule({
    declarations: [
        AppComponent,
        AnalysisFormComponent,
        AnalysisResultsComponent,
        FormControlPipe,
        SwitchOperationModeDialogComponent,
        CopySnackbarComponent,
        HistoryViewComponent,
        HistoryViewItemComponent,
        SummarizeTextPipe,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatButtonModule,
        MatListModule,
        MatInputModule,
        MatButtonToggleModule,
        MatIconModule,
        MatTooltipModule,
        MatSortModule,
        MatDialogModule,
        MatSnackBarModule,
        MatPaginatorModule
    ],
    providers: [
        {
            provide: TextAnalysisService,
            useFactory: (httpClient: HttpClient) => {
                return new TextAnalysisService(
                    new OnlineTextAnalysisService(httpClient),
                    new OfflineTextAnalysisService()
                )
            },
            deps: [HttpClient]
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
